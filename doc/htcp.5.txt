// Copyright (C) 2018 CNRS, |Meso|Star>, Université Paul Sabatier
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
:toc:

htcp(5)
=======

NAME
----
htcp - High-Tune: Cloud Properties file format

DESCRIPTION
-----------

*htcp* is a binary file format that describes properties on liquid water
content in suspension within clouds. The *les2htcp*(1) command can be used to
generate a *htcp* file from cloud properties saved in a NetCDF file format.

The cloud properties are actually spatio temporal double precision floating
point data, structured in a 4D grid whose definition and spatial origin is
given by the _<definition>_ and the _<lower-pos>_ fields. The size of a grid
cell along the X and Y dimension is constant while the size along the Z axis
can be irregular. Is such case, the _<is-Z-irregular>_ header flag is set to 1
and the sizes of the cells in Z are explicitly listed in _<voxel-size>_.  Note
that in *htcp*, the spatial dimensions are defined in meters, i.e. values
listed in _<lower-pos>_ and _<voxel-size>_ are meters.

For a given property, the list of its spatio temporal data are linearly listed
along the X, Y, Z and time dimension, in that order. The address where its
first data is stored is aligned on the value defined by the file header field
_<pagesize>_; several bytes of padding can be thus added priorly to a property
in order to ensure that its first data is correctly aligned.

Available cloud properties are:

* _<RVT>_: water vapor mixing ratio in kg of water per m^3 of dry air.
* _<RCT>_: liquid water in suspension mixing ratio in kg of water per m^3 of
  dry air.
* _<PABST>_: pressure in Pascal.
* _<T>_: temperature in Kelvin.

BINARY FILE FORMAT
------------------

Data are encoded with respect to the little endian bytes ordering, i.e. least
significant bytes are stored first.

[verse]
-------
<htcp>       ::= <header>
                 <definition>
                 <lower-pos>
                 <voxel-size>
                 <padding>
                 <RVT>
                 <padding>
                 <RCT>
                 <padding>
                 <PABST>
                 <padding>
                 <T>
                 <padding>

<header>     ::= <pagesize> <is-Z-irregular>
<definition> ::= <X> <Y> <Z> <time>    # Spatial and temporal definition
<lower-pos>  ::= DOUBLE DOUBLE DOUBLE  # Spatial origin of the grid
<voxel-size> ::= DOUBLE DOUBLE         # Size of a cell in X and Y
                 DOUBLE [ DOUBLE ... ] # Size of the cells in Z

<pagesize>   ::= INT64
<is-Z-irregular>
             ::= INT8

<X>          ::= INT32
<Y>          ::= INT32
<Z>          ::= INT32
<time>       ::= INT32

<RVT>        ::= DOUBLE [ DOUBLE ... ] # Water vapor mixing ratio
<RCT>        ::= DOUBLE [ DOUBLE ... ] # Liquid water in suspension mixing ratio
<PABST>      ::= DOUBLE [ DOUBLE ... ] # Pressure
<T>          ::= DOUBLE [ DOUBLE ... ] # Temperature

<padding>    ::= [ BYTE ... ]
-------

SEE ALSO
--------
*les2htcp*(1)
